<?php

namespace Drupal\commerce_stripe_alipay\Event;

/**
 * Defines events for the Commerce Stripe Alipay module.
 */
final class CommerceStripeAlipayEvents {
  /**
   * Name of the event fired when payment succeeded.
   *
   * @Event
   *
   * @see \Drupal\commerce_stripe_alipay\Event\CommerceStripeAlipayEvent
   */
  const COMMERCE_STRIPE_ALIPAY_PAYMENT_SUCCEEDED = 'commerce_stripe_alipay.payment_succeeded';

  /**
   * Name of the event fired when payment failed.
   *
   * @Event
   *
   * @see \Drupal\commerce_stripe_alipay\Event\CommerceStripeAlipayEvent
   */
  const COMMERCE_STRIPE_ALIPAY_PAYMENT_FAILED = 'commerce_stripe_alipay.payment_failed';

}
