# Commerce Stripe Alipay
INTRODUCTION
------------
This module is a standalone integration of Alipay as a Stripe Wallet

REQUIREMENTS
------------
    - drupal/core: ^8.8 || ^9
    - drupal/commerce : ^2.19
    - stripe/stripe/php: ^7.25

INSTALLATION
------------
Add this module via composer
```
composer require 'drupal/commerce_stripe_alipay:^1.0'
```

CONFIGURATION
------------
 - Go to admin/commerce/config/payment-gateways
 - Click on add a new payment gateway.
 - Find Commerce Alipay Stripe
 - Add your Stripe credentials
